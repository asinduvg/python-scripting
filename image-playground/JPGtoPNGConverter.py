import sys
import os
from PIL import Image

if len(sys.argv) == 3:
    first  = sys.argv[1]
    second = sys.argv[2]

    os_path = os.path.dirname(os.path.abspath(__file__))
    new_path = f'{os_path}\{second}'

    if not os.path.exists(new_path):
        os.mkdir(new_path)

    loop_path = f'{os_path}\{first}'

    for entry in os.scandir(loop_path):
        if entry.path.endswith('.jpg'):
            name = os.path.basename(entry.path).partition('.jpg')[0]
            img = Image.open(entry.path)
            img.save(f'{new_path}\{name}.png', 'png')
